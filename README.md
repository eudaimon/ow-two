# Ow Two

This is an evolution of the venerable KDE Oxygen icon theme.

Mainly it only *adds* icons to it, and has Oxygen, hicolor and Breeze as dependency (see the `Inherits` property in `index.theme`). So you need to have Oxygen icon theme installed!

The goal is to widen the scope of supported apps and especially mime-types, while not recreating an entire icon theme. Oxygen is an old theme and as far as I know, no new icons have been added for a long time, so its shiny and glossy appearance is less and less present over time.

For now I don't indend for it to be a very exhaustive theme (like my [Buuf](https://git.disroot.org/eudaimon/buuf-nestort)). I just want to add some icons that I like to see in my desktop when I'm on retro themes, and that Oxygen does not provide.

For now the icons that I'm adding are scalable SVGs (no multiresolution). Sadly, Plasma ignores blurring of SVG objects and masks, which breaks shadows and some other features of original Oxygen SVGs. That could be solved by rendering PNGs at multiple resolutions, but for now I don't want to invest the time to make it work (original Oxygen already has this thing implemented, so I wouldn't be reinventing the wheel, though).
